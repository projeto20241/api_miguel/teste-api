const router = require('express').Router()
const teacherController = require('../controller/teacherController')
const alunoController = require('../controller/alunoController')
const JSONPlaceholderConteoller = require ('../controller/JSONPlaceholderController')

router.get('/teacher/', teacherController.getTeacher)
router.post('/cadastroaluno/', alunoController.postAluno)
router.put('/updateuser/', alunoController.updateUser)
router.delete('/deleteuser/:id', alunoController.deleteUser)
router.get("/external/", JSONPlaceholderConteoller.getUsers)
router.get("/external/io", JSONPlaceholderConteoller.getUsersWebsiteIO)
router.get("/external/com", JSONPlaceholderConteoller.getUsersWebsiteCOM)
router.get("/external/net", JSONPlaceholderConteoller.getUsersWebsiteNET)


module.exports = router